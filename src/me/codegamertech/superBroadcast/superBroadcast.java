package me.codegamertech.superBroadcast;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class superBroadcast extends JavaPlugin implements Listener {
	private File configf;
    public FileConfiguration config;
    
	private static superBroadcast instance;
	public static superBroadcast getInstance() {return instance;}
	private static String pluginPrefix = "&6&lsuperBroadcast » &3&l&o";
	
	@Override
	public void onEnable() {
		instance = this;
		
		Bukkit.getPluginCommand("bc").setExecutor(new broadcastCommand());
		Bukkit.getPluginCommand("broadcast").setExecutor(new broadcastCommand());
		createFiles();
		Bukkit.getPluginManager().registerEvents(this, this);
		
		Bukkit.getLogger().info("Did you know that, superBroadcast is enabled?");
		
	}
	
	@EventHandler
	public static void playerJoinEvent(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		if(p.getName().equalsIgnoreCase("catman2014_")) {
			e.setJoinMessage(prefixFormat("&6&lcatman2014_&3&l, the creator of superBroadcast has Joined the server."));
			return;
		}
	}
	
	public static String prefixFormat(String text) {
		return ChatColor.translateAlternateColorCodes('&', pluginPrefix + text);
	}
	
	public static String standardFormat(String text) {
		return ChatColor.translateAlternateColorCodes('&', superBroadcast.getInstance().getConfig().getString("prefix") + text);
	}
	
	public FileConfiguration getConfig() {
        return this.config;
    }

    private void createFiles() {

        configf = new File(superBroadcast.getInstance().getDataFolder(), "config.yml");

        if (!configf.exists()) {
            configf.getParentFile().mkdirs();
            superBroadcast.getInstance().saveResource("config.yml", false);
        }

        config = new YamlConfiguration();
        try {
            config.load(configf);
        } catch (IOException e) {
        	Bukkit.getLogger().warning("A Internal Error Was Occured, Here is the Stack Trace:");
			e.printStackTrace();
        } catch (InvalidConfigurationException e) {
        	Bukkit.getLogger().warning("A Internal Error Was Occured, Here is the Stack Trace:");
			e.printStackTrace();
		}
    }
	
}
