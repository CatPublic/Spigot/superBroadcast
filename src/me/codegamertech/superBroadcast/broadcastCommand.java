package me.codegamertech.superBroadcast;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class broadcastCommand implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
		if(cmd.getName().equalsIgnoreCase("broadcast") || cmd.getName().equalsIgnoreCase("bc")) {
			if(sender.hasPermission("superBroadcast.broadcast")) {
				if(args.length >= 1) {
					if(args[0].equalsIgnoreCase("chat")) {
						StringBuilder sb = new StringBuilder();
						for (int i = 1; i < args.length; i++){
						sb.append(args[i]).append(" ");
						}
						 
						String message = sb.toString().trim();
						
						for(Player p : Bukkit.getOnlinePlayers()){
				            p.sendMessage(superBroadcast.standardFormat(message));
				        }
						
						return true;
					} else if(args[0].equalsIgnoreCase("title")) {
						StringBuilder sb = new StringBuilder();
						for (int i = 1; i < args.length; i++){
						sb.append(args[i]).append(" ");
						}
						 
						String message = sb.toString().trim();
						
						for(Player p : Bukkit.getOnlinePlayers()){
							sendTitle.sendFullTitle(p, 5, superBroadcast.getInstance().getConfig().getInt("title-stay-length") * 20, 5, superBroadcast.getInstance().getConfig().getString("title-header"), message);
				        }
						
						return true;
					}
					
					return true;
				} else {
					sender.sendMessage(superBroadcast.prefixFormat("usage: /bc <title/chat> <message>"));
					return true;
				}
			} else {
				sender.sendMessage(superBroadcast.prefixFormat("permission node 'superBroadcast.broadcast' needed to use this command!"));
				return true;
			}
		}
		return false;
	}

}
